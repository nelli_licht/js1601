(function () {
    'use strict';

    var app = angular.module('app', ['ngRoute']);

    app.constant('baseUrl', window.location.origin);

    app.config(['$locationProvider', function($locationProvider) {
        $locationProvider.hashPrefix('');
    }]);

})();