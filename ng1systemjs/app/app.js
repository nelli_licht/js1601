var AddCtrl = require ('./add.ctrl.js');
var DataSrv = require ('./data.srv.js');
var ListCtrl = require ('./list.ctrl.js');
var Routes = require ('./routes.js');

var app = angular
          .module('app', ['ngRoute'])
          .controller('AddCtrl', AddCtrl)
          .service('DataSrv', DataSrv)
          .controller('ListCtrl', ListCtrl)
          .config(Routes);


angular.element(document).ready(function() {
    angular.bootstrap(document, ['app']);
});

app.constant('baseUrl', window.location.origin);

app.config(['$locationProvider', function($locationProvider) {
    $locationProvider.hashPrefix('');
}]);
