/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	var AddCtrl = __webpack_require__ (1);
	var DataSrv = __webpack_require__ (2);
	var ListCtrl = __webpack_require__ (3);
	var Routes = __webpack_require__ (4);

	var app = angular
	          .module('app', ['ngRoute'])
	          .controller('AddCtrl', AddCtrl)
	          .service('DataSrv', DataSrv)
	          .controller('ListCtrl', ListCtrl)
	          .config(Routes);


	angular.element(document).ready(function() {
	    angular.bootstrap(document, ['app']);
	});

	app.constant('baseUrl', window.location.origin);

	app.config(['$locationProvider', function($locationProvider) {
	    $locationProvider.hashPrefix('');
	}]);


/***/ },
/* 1 */
/***/ function(module, exports) {

	    Ctrl.$inject = ['$location', 'data'];

	    function Ctrl($location, data) {

	        var vm = this;
	        vm.title = '';
	        vm.text = '';
	        vm.addNew = addNew;

	        function addNew() {
	            var newPost = {
	                title : vm.title,
	                text : vm.text
	            };

	            data.addPost(newPost).then(function (a) {
	                $location.path('/list');
	            });
	        }
	    }

	module.exports = Ctrl;



/***/ },
/* 2 */
/***/ function(module, exports) {

	    Srv.$inject = ['$q'];

	    function Srv($q) {

	        var srv = this;

	        srv.addPost = addPost;
	        srv.getPosts = getPosts;

	        var posts = [
	            {
	                title: "Post 1"
	            },
	            {
	                title: "Post 2"
	            }
	        ];

	        function addPost(post) {
	            posts.push(post);

	            return $q.resolve();
	        }

	        function getPosts() {
	            return $q.resolve(posts);
	        }

	    }


	module.exports = Srv;


/***/ },
/* 3 */
/***/ function(module, exports) {

	    Ctrl.$inject = ['data'];

	    function Ctrl(data) {

	        var vm = this;
	        vm.posts = [];

	        init();

	        function init() {
	            data.getPosts().then(function (result) {
	                vm.posts = result;
	            });
	        }

	    }


	module.exports = Ctrl;

/***/ },
/* 4 */
/***/ function(module, exports) {

	    Conf.$inject = ['$routeProvider'];

	    function Conf($routeProvider) {

	        $routeProvider.when('/list', {
	            templateUrl : 'app/list.html',
	            controller : 'ListCtrl',
	            controllerAs : 'vm'
	        }).when('/new', {
	            templateUrl : 'app/add.html',
	            controller : 'AddCtrl',
	            controllerAs : 'vm'
	        }).otherwise('/list');
	    }


	module.exports = Conf;

/***/ }
/******/ ]);