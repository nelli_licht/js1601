import { Routes } from '@angular/router';

import { SearchComponent } from './search.cmp';
import { AddComponent } from './add.cmp';

export const routes: Routes = [
    { path: 'search', component: SearchComponent },
    { path: 'add', component: AddComponent },
    { path: '', redirectTo: 'search', pathMatch: 'full' }
];