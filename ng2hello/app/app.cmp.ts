import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Post } from './post.cls.ts';


@Component({
    selector: 'my-app',
    templateUrl: 'app/app.html'
})
export class AppComponent {

private posts: Post [] = [];
private newPostTitle: string = '';


constructor(private http: Http){
     this.getPosts();

}
        getPosts(){
             this.http.get('api/posts')
                    .toPromise()
                    .then((response) => this.posts = response.json());

        }

        addNewPost(){
         this.http.post('api/posts', new Post(this.newPostTitle, ''))
         .toPromise()
         .then(() => this.getPosts());
         }
}
