(function () {
    'use strict';

    angular.module('app').controller('AddCtrl', Ctrl);

    function Ctrl( HelloService , $location ){

        var ctrl = this;
        ctrl.title = "";
        ctrl.text = "";
        ctrl.addPost = function() {
            savePost();
            ctrl.title = "";
            ctrl.text = "";
        };


        function savePost() {
            HelloService.savePost(ctrl.title, ctrl.text).then( function(){
                $location.path('list');
            });

        }



    }


})();
