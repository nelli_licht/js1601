(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('ListCtrl', Ctrl);

    function Ctrl( HelloService ) {
        var ctrl = this;
        ctrl.title = 'List';
        ctrl.posts = [];

    function listPosts(){
       HelloService.loadPosts().then (function(posts) {
       ctrl.posts = posts;
        });
    }

    listPosts();
    }




})();
