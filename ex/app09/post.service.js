(function () {
    'use strict';

    angular.module('app').service('HelloService', HelloService);

    function HelloService( $http ){
        console.log('Hello');

        this.savePost = function(title, text) {

            var post1 = {
                title: title,
                text: text
            };

            return $http.post('api/posts', post1);
        }


            this.loadPosts = function(){
                return $http.get('api/posts').then (function(result) {
                return result.data;
                });
            }
    }





})();
