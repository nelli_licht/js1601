import { Pipe, Injectable, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter'
})
@Injectable()
export class FilterPipe implements PipeTransform {
    transform(items: any[], value : string): any[] {
        if (!items) return [];

        return items.filter(each => {
            return this.matches(each.firstName, value)
                    || this.matches(each.lastName, value)
                    || this.matches(each.code, value);
        });
    }

    private matches(item : any, searchString : string) {
        if (!searchString || searchString.length === 0) {
            return true;
        }

        return item.toLowerCase().indexOf(searchString.toLowerCase()) >= 0;
    }

}
