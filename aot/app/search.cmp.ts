import 'rxjs/add/operator/toPromise';

import { Component } from '@angular/core';
import { Http }    from '@angular/http';
import { Customer } from "./model/customer.cls";

@Component({
    selector: 'search',
    templateUrl: 'search.html'
})
export class SearchComponent {

    searchString = '';
    customers: Customer[] = [];

    constructor(private http: Http) {
        this.getData();
    }

    private getData() {
        this.http.get('api/customers')
            .toPromise()
            .then((response) => this.customers = response.json());
    }

    private deleteCustomer(customer : Customer) {
        event.preventDefault();

        this.http.delete('api/customers/' + customer.id)
            .toPromise()
            .then(() => this.getData());
    }

}
