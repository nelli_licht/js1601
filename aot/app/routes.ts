import { Routes } from '@angular/router';
import { SearchComponent } from './search.cmp';
import { EditComponent } from './edit.cmp';

export const routes: Routes = [
    { path: 'search', component: SearchComponent },
    { path: 'new', component: EditComponent },
    { path: 'edit/:id', component: EditComponent },
    { path: '', redirectTo: 'search', pathMatch: 'full' }
];