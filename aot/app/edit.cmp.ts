import 'rxjs/add/operator/toPromise';

import { Component } from '@angular/core';
import { Http }    from '@angular/http';
import { Router } from '@angular/router'
import { ActivatedRoute } from '@angular/router';
import { Customer } from "./model/customer.cls";
import { Phone } from "./model/phone.cls";


@Component({
    selector: 'edit',
    templateUrl: 'edit.html'
})
export class EditComponent {

    errors: any = [];
    customerTypes = ['', 'customer_type.private', 'customer_type.corporate'];
    phoneTypes = ['', 'phone_type.fixed', 'phone_type.mobile'];
    customer: Customer = new Customer();

    private id: string;
    private sub: any;

    constructor(private http: Http,
                private route: ActivatedRoute,
                private router: Router) { }

    public submitForm(event : any): void {
        event.preventDefault();

        this.errors = [];

        this.http.post('api/customers', this.customer)
            .toPromise()
            .then(() => this.router.navigateByUrl('search'))
            .catch((response) => this.handleError(response));

    }

    public addPhone(): void {
        event.preventDefault();

        this.customer.phones.push(new Phone());
    }

    public deletePhone(phone : Phone): void {
        event.preventDefault();

        this.customer.phones = this.customer.phones.filter(p => p !== phone);
    }

    private handleError(response : any): Promise<any> {
        this.errors = [];

        var errors = response.json().errors;

        if (errors === undefined) {
            return;
        }

        errors.forEach((e : any) => {
            var error = {
                code : e.code,
                message : e.code
            };

            this.errors.push(error);
        });
    }

    private loadData(id: string) {
        this.http.get('api/customers/' + id)
            .toPromise()
            .then((data) => this.customer = data.json());
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            var id = params['id'];
            if (id) {
                this.loadData(id);
            }
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }
}
