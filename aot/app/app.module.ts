import { NgModule }      from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule }    from '@angular/http';

import { AppComponent } from './app.cmp';
import { SearchComponent } from './search.cmp';
import { EditComponent } from './edit.cmp';
import { FilterPipe } from './filter';
import {routes} from './routes';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(routes, { useHash: true })
    ],
    declarations: [AppComponent, SearchComponent, EditComponent, FilterPipe],
    bootstrap: [AppComponent]
})
export class AppModule {
}
