import {Phone} from "./phone.cls";

export class Customer {
    id: string;
    firstName: string;
    lastName: string;
    code: string;
    type: string;
    phones: Phone[] = [];
}
