"use strict";
var phone_1 = require("./phone");
var Person = (function () {
    function Person(name) {
        this.name = name;
    }
    Person.prototype.getName = function () {
        return this.name;
    };
    return Person;
}());
console.log(new Person('Jack').getName());
console.log(new phone_1.Phone(123));
